<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $lastName;

    /**
     * @ORM\Column(type="date")
     */
    private $dateOfBirth;

    /**
     * @ORM\OneToOne(targetEntity=Gender::class, inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $gender;

    /**
     * @ORM\OneToOne(targetEntity=BloodGroup::class, inversedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $bloodGroup;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity=Education::class, mappedBy="user", orphanRemoval=true)
     */
    private $education;

    /**
     * @ORM\OneToMany(targetEntity=MobileNumber::class, mappedBy="user", orphanRemoval=true)
     */
    private $mobileNumber;

    /**
     * @ORM\OneToMany(targetEntity=EmailAddress::class, mappedBy="user", orphanRemoval=true)
     */
    private $emailAddresses;

    public function __construct()
    {
        $this->education = new ArrayCollection();
        $this->mobileNumber = new ArrayCollection();
        $this->emailAddresses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBloodGroup(): ?BloodGroup
    {
        return $this->bloodGroup;
    }

    public function setBloodGroup(BloodGroup $bloodGroup): self
    {
        $this->bloodGroup = $bloodGroup;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection|Education[]
     */
    public function getEducation(): Collection
    {
        return $this->education;
    }

    public function addEducation(Education $education): self
    {
        if (!$this->education->contains($education)) {
            $this->education[] = $education;
            $education->setUser($this);
        }

        return $this;
    }

    public function removeEducation(Education $education): self
    {
        if ($this->education->removeElement($education)) {
            // set the owning side to null (unless already changed)
            if ($education->getUser() === $this) {
                $education->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MobileNumber[]
     */
    public function getMobileNumber(): Collection
    {
        return $this->mobileNumber;
    }

    public function addMobileNumber(MobileNumber $mobileNumber): self
    {
        if (!$this->mobileNumber->contains($mobileNumber)) {
            $this->mobileNumber[] = $mobileNumber;
            $mobileNumber->setUser($this);
        }

        return $this;
    }

    public function removeMobileNumber(MobileNumber $mobileNumber): self
    {
        if ($this->mobileNumber->removeElement($mobileNumber)) {
            // set the owning side to null (unless already changed)
            if ($mobileNumber->getUser() === $this) {
                $mobileNumber->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmailAddress[]
     */
    public function getEmailAddresses(): Collection
    {
        return $this->emailAddresses;
    }

    public function addEmailAddress(EmailAddress $emailAddress): self
    {
        if (!$this->emailAddresses->contains($emailAddress)) {
            $this->emailAddresses[] = $emailAddress;
            $emailAddress->setUser($this);
        }

        return $this;
    }

    public function removeEmailAddress(EmailAddress $emailAddress): self
    {
        if ($this->emailAddresses->removeElement($emailAddress)) {
            // set the owning side to null (unless already changed)
            if ($emailAddress->getUser() === $this) {
                $emailAddress->setUser(null);
            }
        }

        return $this;
    }
}
