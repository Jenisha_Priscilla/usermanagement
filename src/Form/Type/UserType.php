<?php
namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\Type\GenderType;
use App\Form\Type\BloodGroup;
use App\Form\Type\EducationType;
use App\Form\Type\MobileNumberType;
use App\Form\Type\EmailAddressType;
use App\Entity\User;

class UserType extends AbstractType 
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('dateOfBirth', DateType::class)
            ->add('gender', GenderType::class)
            ->add('bloodGroup', BloodGroupType::class)
            ->add('education', CollectionType::class, [
                'entry_type' => EducationType::class,
                'allow_add' => true,
            ])
            ->add('mobileNumber', CollectionType::class, [
                'entry_type' => MobileNumberType::class,
                'allow_add' => true,
            ])
            ->add('emailAddresses', CollectionType::class, [
                'entry_type' => EmailAddressType::class,
                'allow_add' => true,
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}