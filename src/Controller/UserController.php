<?php
namespace App\Controller;
use App\Controller\UserBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\User;

class UserController extends UserBaseController
{
    public function addUser(Request $request)
    {
        $data = $request->toArray();
        $user = new User();
        $this->userForm($user, $data, $request);
    }
}