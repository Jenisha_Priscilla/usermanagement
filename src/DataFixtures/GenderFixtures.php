<?php

namespace App\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Gender;
use DateTime;

class GenderFixtures extends Fixture
{
    private $gender = [
        'male',
        'female'
    ];
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        array_map(function ($value){
            $gender = new Gender();
            $gender->setGender($value);
            $gender->setCreatedAt(new DateTime("now"));
            $gender->setUpdateAt(new DateTime("now"));
            $this->manager->persist($gender);
        },$this->gender);

        $this->manager->flush();
    }
}
