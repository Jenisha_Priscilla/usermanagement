<?php

namespace App\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\BloodGroup;
use DateTime;

class BloodGroupFixtures extends Fixture
{
    private $bloodGroup = [
        'A +ve',
        'A -ve',
        'B +ve',
        'B -ve',
        'O +ve',
        'O -ve',
        'AB +ve',
        'AB -ve'
    ];
    private $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        array_map(function ($value){
            $bloodGroup = new BloodGroup();
            $bloodGroup->setBloodGroup($value);
            $bloodGroup->setCreatedAt(new DateTime("now"));
            $bloodGroup->setUpdateAt(new DateTime("now"));
            $this->manager->persist($bloodGroup);
        },$this->bloodGroup);

        $this->manager->flush();
    }
}
